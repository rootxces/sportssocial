package com.example.rootxces.ssinterviewapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rootxces.ssinterviewapp.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import com.example.rootxces.ssinterviewapp.pojo.Feed;
import com.squareup.picasso.Picasso;

public class FeedRecyclerViewAdapter extends RecyclerView.Adapter<FeedRecyclerViewAdapter.MyViewHolder> {

    ArrayList<Feed> mList;
    Context context;

    public FeedRecyclerViewAdapter(ArrayList<Feed> listOfFeed, Context context){
        this.mList = listOfFeed;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        CircleImageView profilePicture;
        ImageView eventImage;
        TextView username, userAction, eventTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            profilePicture = itemView.findViewById(R.id.image_author);
            eventImage = itemView.findViewById(R.id.event_image);
            username = itemView.findViewById(R.id.author_name);
            userAction = itemView.findViewById(R.id.author_action);
            eventTime = itemView.findViewById(R.id.event_time);
        }
    }

    @NonNull
    @Override
    public FeedRecyclerViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.feed_item, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedRecyclerViewAdapter.MyViewHolder myViewHolder, int i) {

        Feed feed = mList.get(i);
        myViewHolder.username.setText(feed.getUsername());
        myViewHolder.userAction.setText(feed.getUserAction());
        myViewHolder.eventTime.setText(feed.getEventTime());

        Picasso.get()
                .load(feed.getUserImageUrl())
                .placeholder(R.drawable.ic_broken_image)
                .error(R.drawable.ic_broken_image)
                .into(myViewHolder.profilePicture);
        Picasso.get().load(feed.getEventImageUrl())
                .placeholder(R.drawable.ic_broken_image)
                .error(R.drawable.ic_broken_image)
                .into(myViewHolder.eventImage);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
