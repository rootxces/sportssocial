package com.example.rootxces.ssinterviewapp.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rootxces.ssinterviewapp.R;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import com.example.rootxces.ssinterviewapp.adapter.FeedRecyclerViewAdapter;
import com.example.rootxces.ssinterviewapp.pojo.Feed;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OpenArenaFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private FeedRecyclerViewAdapter mAdapter;
    private ArrayList<Feed> feedList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_open_arena, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        populateDummyData();
        feedList = new ArrayList<>();
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mAdapter = new FeedRecyclerViewAdapter(feedList, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

    }

    public void populateDummyData(){
        feedList = new ArrayList<>();

        Feed obj1 = new Feed();
        obj1.setUsername("Kapil Pokhriyal");
        obj1.setUserAction("Started a basketball match");
        obj1.setEventTime("20 min ago");
        obj1.setUserImageUrl("https://test.sportsocial.in/venueimages/0.6959999622969562.jpeg");
        obj1.setEventImageUrl("https://test.sportsocial.in/images/0.6299647129592791.jpeg");
        feedList.add(obj1);

        Feed obj2 = new Feed();
        obj2.setUsername("Mark Samuel");
        obj2.setUserAction("Started a basketball match");
        obj2.setEventTime("14 min ago");
        obj2.setUserImageUrl("https://test.sportsocial.in/venueimages/0.7359801757617472.jpeg");
        obj2.setEventImageUrl("https://test.sportsocial.in/images/0.8906383069389945.jpeg");
        feedList.add(obj2);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new FetchFeeds().execute("https://test.sportsocial.in/testfeed");
    }

    private class FetchFeeds extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            URL url = null;
            StringBuilder total = new StringBuilder();
            try {
                url = new URL(params[0].toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection urlConnection = null;
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            return total.toString();
        }

        @Override
        protected void onPostExecute(String result) {
//            System.out.print(result);
            if (result != null) {
                try {
//                    JSONObject rootObj = new JSONObject(result);
                    JSONArray rootArray = new JSONArray(result);
                    for (int i = 0; i < rootArray.length(); i++) {
                        JSONObject c = rootArray.getJSONObject(i);

                        Feed feed = new Feed();
                        feed.setUsername(c.getString("user_name"));
                        feed.setUserAction("started a "+c.getString("gamename")+"match");
                        feed.setEventTime("few min ago");
                        feed.setEventImageUrl(c.getString("event_image"));
                        feed.setUserImageUrl(c.getString("profile_image"));
                        feedList.add(feed);
                    }
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {
                System.out.print("kdf");
            }

        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

}
