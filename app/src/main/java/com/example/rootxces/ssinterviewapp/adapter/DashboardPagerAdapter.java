package com.example.rootxces.ssinterviewapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.rootxces.ssinterviewapp.fragments.MyArenaFragment;
import com.example.rootxces.ssinterviewapp.fragments.OpenArenaFragment;

public class DashboardPagerAdapter extends FragmentStatePagerAdapter {

    public DashboardPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override    public Fragment getItem(int position) {
        switch (position){
            case 0: return new OpenArenaFragment();
            case 1: return new MyArenaFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override    public CharSequence getPageTitle(int position) {        switch (position){
        case 0: return "Open Arena";
        case 1: return "My Arena";
        default: return null;
    }
    }
}


